var Comment = {

    /**
     * Performs AJAX request for comment addition
     *
     * @param targ        Form DOM element
     * @returns {Boolean}
     */
    add: function (targ) {
        $.ajax({
            url:  $(targ).attr('action'),
            data: $(targ).serialize(),
            type: 'post',
            context: $(targ)
        }).done(function (resp) {
            var $comment = $(this).parents('.comment');

            // Reply
            if ($comment.length > 0) {
                // Actually should be inserted not right after current comment, but at the end of the same level siblings
                $(resp).insertAfter($comment);
                $comment.find('.formWrap').hide();
            } else {
                $(resp).prependTo('#comments');
            }

            $(this).parents('form').find('input, textarea').val('');
        });

        return false;
    },

    /**
     * Toggles comment reply form
     *
     * @param targ Reply link
     */
    reply: function (targ) {
        $(targ).parent().find('.formWrap').toggle();

        return false;
    },

    /**
     * Edit form
     *
     * @param targ Edit link
     * @returns {boolean}
     */
    edit: function (targ) {
        // Not implemented, no time =(

        return false
    }
};

$(function () {
    $( document ).ajaxError(function (_, error) {
        $('#error').html(error.responseText);

        setTimeout(function () {
            $('#error').html('');
        }, 3000);
    });
});
