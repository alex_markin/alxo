<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Components\DB;

// Parse the dotenv file
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

// Initialize database connection
try {
    DB::init(getenv('DB_DRIVER'), getenv('DB_HOST'), getenv('DB_USERNAME'),
        getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
} catch (PDOException $e) {
    echo "Couldn't connect to database: " . $e->getMessage();
    exit;
}

// Routes list. Contains controller and an action
$routes = [
    '/create' => 'App\Controllers\CommentController@create',
    '/edit'   => 'App\Controllers\CommentController@edit',
    '/delete' => 'App\Controllers\CommentController@delete',
    '/'       => 'App\Controllers\CommentController@index',
];

// Iterate through routes trying to find a match
foreach ($routes as $route => $handler) {
    if (strpos($_SERVER['REQUEST_URI'], $route) === 0) {
        list($controller, $action) = explode('@', $handler);

        try {
            echo (new $controller)->$action();
        } catch (Exception $e) {
            http_response_code($e->getCode());
            echo $e->getMessage();
        }

        exit;
    }
}

header("HTTP/1.0 404 Not Found");
echo 'Not found';
