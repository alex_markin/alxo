DROP TABLE IF EXISTS comments;
CREATE TABLE comments (
    id INT(10) NOT NULL AUTO_INCREMENT,
    lft INT(10) NOT NULL DEFAULT 0,
    rgt INT(10) NOT NULL DEFAULT 0,
    level INT(10) NOT NULL DEFAULT 0,
    name VARCHAR(32) NOT NULL DEFAULT '',
    comment TEXT NOT NULL DEFAULT '',
    PRIMARY KEY (id),
    INDEX left_key (lft, rgt, level)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO comments (comment, lft, rgt, level) VALUES ('', 1, 2, 1);