<?php

namespace App\Controllers;

class BaseController
{
    const TEMPLATES_PATH = __DIR__ . '/../Views/';

    /**
     * Renders passed template
     *
     * @param string $template Template name
     * @param array  $data     Data to be passed into template
     * @return string
     */
    protected function render(string $template, array $data = []) : string
    {
        ob_start();

        // Performs extracting passed array to variables
        extract($data, EXTR_SKIP);

        require static::TEMPLATES_PATH . "$template.php";

        $output = ob_get_contents();
        ob_end_clean();

        return $output;
    }
}
