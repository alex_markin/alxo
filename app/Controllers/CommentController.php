<?php

namespace App\Controllers;

use App\Exceptions\ValidationException;
use App\Models\Comment;

class CommentController extends BaseController
{
    public function index()
    {
        return $this->render('index', [
            'comments' => Comment::model()->getAll(),
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function create()
    {
        $name    = trim($_POST['name']);
        $comment = trim($_POST['comment']);

        if (!$name || !$comment) {
            throw new ValidationException('Both name and comment are required');
        }

        $commentId = Comment::model()->create((int)$_GET['id'], [
            'name'    => $name,
            'comment' => $comment,
        ]);

        return $this->render('comment_item', [
            'comment' => Comment::model()->getById($commentId),
        ]);
    }
}
