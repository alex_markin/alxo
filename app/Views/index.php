<?php require_once 'layout/header.php'; ?>

<div id="error"></div>

<?php $parentNodeId = 1; ?>
<?php require 'comment_form.php'; ?>

<div id="comments">
    <?php foreach ($comments as $comment): ?>
        <?php require 'comment_item.php'; ?>
    <?php endforeach; ?>
</div>

<?php require_once 'layout/footer.php'; ?>
