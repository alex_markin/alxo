<div class="comment row" style="margin-left: <?php echo ($comment['level'] - 2) * 20; ?>px">
    <strong><?php echo $comment['name']; ?></strong>
    <div class="commentBody"><?php echo $comment['comment']; ?></div>

    <a href="#" onclick="return Comment.reply(this)">Reply</a>
    <a href="#" onclick="return Comment.edit(this)">Edit</a>

    <div class="formWrap" style="display: none">
        <?php $parentNodeId = $comment['id']; ?>
        <?php require 'comment_form.php'; ?>
    </div>
</div>
