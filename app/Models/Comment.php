<?php

namespace App\Models;

class Comment extends BaseModel
{
    /**
     * @var string
     */
    protected $tableName = 'comments';

    /**
     * Returns all comments as a tree
     */
    public function getAll()
    {
        $query = "
            SELECT id, level, name, comment
            FROM $this->tableName
            WHERE level > 1
            ORDER BY lft
        ";

        foreach ($this->select($query) as $comment) {
            yield $comment;
        }
    }

    /**
     * Creates the comment under passed node
     *
     * @param int $parentNodeId
     * @param array $params
     * @return int
     */
    public function create(int $parentNodeId, array $params)
    {
        $query = "
            SELECT @RGT:=rgt, @LVL:=level FROM comments WHERE id = :id;

            UPDATE comments
            SET rgt = rgt + 2, lft = IF(lft > @RGT, lft + 2, lft)
            WHERE rgt >= @RGT;

            INSERT INTO comments
            SET
                lft     = @RGT,
                rgt     = @RGT + 1,
                level   = @LVL + 1,
                name    = :name,
                comment = :comment;
        ";

        $bindings = [
            ':id'      => $parentNodeId,
            ':name'    => $params['name'],
            ':comment' => $params['comment'],
        ];

        return $this->insert($query, $bindings);
    }

    /**
     * Returns comment by its ID
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id)
    {
        $query = "
            SELECT id, level, name, comment
            FROM $this->tableName
            WHERE id = ?
        ";

        return $this->selectOne($query, [$id]);
    }
}
