<?php

namespace App\Models;

use App\Components\DB;
use PDO;
use PDOStatement;

class BaseModel
{
    /**
     * @return BaseModel instance of the current class
     */
    public static function model() : BaseModel
    {
        return new static;
    }

    /**
     * Helper returns Database instance
     *
     * @return PDO
     */
    protected function getDb()
    {
        return DB::getInstance();
    }

    /**
     * Helper performs query with placeholders
     *
     * @param string $query
     * @param array $placeholders
     * @return PDOStatement
     */
    private function execute(string $query, array $placeholders) : PDOStatement
    {
        $sth = $this->getDb()->prepare($query);
        $sth->execute($placeholders);

        return $sth;
    }

    /**
     * Performs PDO SELECT query and return all rows in a result
     *
     * @param string $query
     * @param array $placeholders
     * @return array
     */
    public function select(string $query, array $placeholders = []) : array
    {
        return $this->execute($query, $placeholders)->fetchAll();
    }

    /**
     * Performs PDO SELECT query and return first row
     *
     * @param string $query
     * @param array $placeholders
     * @return array
     */
    public function selectOne(string $query, array $placeholders = []) : array
    {
        return $this->execute($query, $placeholders)->fetch();
    }

    /**
     * Performs INSERT statement and returns last insterted ID
     *
     * @param string $query
     * @param array $placeholders
     * @return int
     */
    public function insert(string $query, array $placeholders = []) : int
    {
        $this->getDb()->beginTransaction();
        $this->execute($query, $placeholders);
        $lastInsertId = $this->getDb()->lastInsertId(); // Doesn't work after commit
        $this->getDb()->commit();

        return (int)$lastInsertId;
    }
}
