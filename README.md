# Install

Run:
```
cp .env.example .env && composer install
```

Change the database connection data in .env

Execute scheme.sql to create database scheme with tree table.
